//
//  URLRequest+Extension.swift
//  trutify-dev
//
//  Created by Alexandre Costa on 20/05/2022.
//

import Foundation

typealias MediaType = URLRequest.MediaType
typealias HeaderField = URLRequest.HeaderField
typealias GrantType = URLRequest.GrantType

extension URLRequest {
    
    /// Content type.
    enum MediaType: String {
        case json = "application/json; charset=utf-8"
        case formURLEncoded = "application/x-www-form-urlencoded" // ; charset=utf-8
    }

    /// Header field.
    enum HeaderField: String {
        case contentType = "Content-Type"
        case accept = "Accept"
        case authorization = "Authorization"
    }

    /// Grant type
    enum GrantType: String, Codable {
        case authorizationCode = "authorization_code"
        case clientCredentials = "client_credentials"
        case refreshToken = "refresh_token"
    }
}
