//
//  UserDefaults+Extensions.swift
//  trutify-dev
//
//  Created by Alexandre Costa on 20/05/2022.
//

import Foundation

extension UserDefaults {
    
    // MARK: - Generic Get and Set
    
    func get<T>(_ key: DefaultsKey<T>) -> T? {
        return object(forKey: key.value) as? T
    }

    func set<T>(_ key: DefaultsKey<T>, to value: T) {
        set(value, forKey: key.value)
        print("\(key) = \(String(describing: value))")
    }
    
    // MARK: - Methods
    
    /// Clear All saved UserDefaults Data
    func clearAll() {
        if let appDomain = Bundle.main.bundleIdentifier {
            UserDefaults.standard.removePersistentDomain(forName: appDomain)
        }
    }
}
