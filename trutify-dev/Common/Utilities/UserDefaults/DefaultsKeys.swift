//
// DefaultsKeys.swift
// Trutify iOS App
//
// Copyright © 2020 Trutify Limited. All rights reserved.
//
// This file may not be copied and/or distributed without the express
// permission of Trutify Limited.
//

import Foundation

/// DefaultsKeys setting the type
class DefaultsKeys {
    
    // Tokens (User Session)
    static let apiAccessToken = DefaultsKey<String>(SessionKeys.apiAccessToken.rawValue)
    static let apiAccessTokenExpirationDate = DefaultsKey<Date>(SessionKeys.apiAccessTokenExpirationDate.rawValue)
    static let apiRefreshToken = DefaultsKey<String>(SessionKeys.apiRefreshToken.rawValue)
    static let userId = DefaultsKey<String>(SessionKeys.userId.rawValue)
    
    // App Mode
    static let appView = DefaultsKey<String>(SessionKeys.appView.rawValue)
    
}
