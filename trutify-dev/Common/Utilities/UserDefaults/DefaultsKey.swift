//
// DefaultsKey.swift
// Trutify iOS App
//
// Copyright © 2020 Trutify Limited. All rights reserved.
//
// This file may not be copied and/or distributed without the express
// permission of Trutify Limited.
//

import Foundation

/// Generic DefaultsKey that will be used as UserDefaults value
final class DefaultsKey<T>: DefaultsKeys {
    let value: String

    init(_ value: String) {
        self.value = value
    }
}
