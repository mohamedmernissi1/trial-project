//
// SessionKeys.swift
// Trutify iOS App
//
// Copyright © 2020 Trutify Limited. All rights reserved.
//
// This file may not be copied and/or distributed without the express
// permission of Trutify Limited.
//

import Foundation

/// Settings Keys
enum SessionKeys: String {

    /// API authorization code.
    case apiAccessToken

    /// API access token expiration date.
    case apiAccessTokenExpirationDate

    /// API refresh token.
    case apiRefreshToken
    
    /// API User Id.
    case userId
    
    // App Mode
    case appView
}
