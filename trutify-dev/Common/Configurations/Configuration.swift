//
//  Configuration.swift
//  trutify-dev
//
//  Created by Alexandre Costa on 20/05/2022.
//

import Foundation

class Configuration {
    
    // MARK: - Properties
    
    private let fileName: String
    private let config: [String: Any]
    
    // MARK: - Init
    
    init(fileName: String) {
        self.fileName = fileName
        
        // Read configuration dictionary from plist
        let path = Bundle.main.path(forResource: fileName, ofType: "plist")
        self.config = NSDictionary(contentsOfFile: path!) as! [String: Any]
    }
    
    // MARK: - Public Interface
    
    func value<T>(forKey key: String, isEnvironmentValue: Bool = false) -> T {
        let dictionary = isEnvironmentValue ? baseURLDictionary : config
        
        if let value = dictionary[key] as? T {
            return value
        } else {
            fatalError("Value for key \(key) not found in \(fileName).plist")
        }
    }
    
    // MARK: - Local helpers
    
    private var baseURLDictionary: [String: Any] {
        #if DEBUG
        return self.config["DEV"] as! [String: Any]
        #else
        return self.config["PROD"] as! [String: Any]
        #endif
    }
}
