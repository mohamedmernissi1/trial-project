//
//  NetworkingAPIConfiguration.swift
//  trutify-dev
//
//  Created by Alexandre Costa on 20/05/2022.
//

import Foundation

// MARK: - Networking API Configuration

class NetworkingAPIConfiguration: Configuration {
    
    // MARK: - Init
    
    init() {
        super.init(fileName: "NetworkingAPIConfiguration")
    }
    
    // MARK: - URLs
    var host: String { value(forKey: "host", isEnvironmentValue: true)}
    
    // MARK: - Client Information
    var clientId: String { value(forKey: "clientId", isEnvironmentValue: true) }
    var clientSecret: String { value(forKey: "clientSecret", isEnvironmentValue: true) }
    
    var tokens: String{ value(forKey: "tokens") }
    var rewards: String{ value(forKey: "/v0.1/rewards-service/rewards") }
}
