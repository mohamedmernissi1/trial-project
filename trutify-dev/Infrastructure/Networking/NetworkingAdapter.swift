//
//  NetworkingAdapter.swift
//  trutify-dev
//
//  Created by Alexandre Costa on 20/05/2022.
//

import Foundation
import Moya

struct NetworkingAdapter {

    // MARK: - Properties

    private var logOptions: NetworkLoggerPlugin.Configuration.LogOptions {
        #if DEVELOPMENT
        return .verbose
        #else
        return .default
        #endif
    }

    // MARK: - Request

    /// Request Method for generic model conforming to Decodable
    /// - Parameters:
    ///   - target: Networking API
    ///   - decodingType: Model
    ///   - completion: Completion Result, either the response or an error
    func request<T: Decodable>(target: NetworkingAPI, decodingType: T.Type, completion: @escaping (Result<T, APIError>) -> Void) {

        // Init the provider
        let provider = MoyaProvider<NetworkingAPI>(plugins: [NetworkLoggerPlugin(configuration: .init(logOptions: self.logOptions))])

        provider.request(target) { (result) in

            print("\n--RQ: target:: \(target)")

            switch result {
            case .success(let response):
                if response.statusCode == 201 {
                    // Creation Success - Send Id of new resource
                    let location = response.response?.headers.value(for: "location")!.split(separator: "/")
                    let resourceId = String(location![location!.count - 1])
                    completion(.success(resourceId as! T))
                }
                else if response.statusCode >= 200 && response.statusCode < 300 {
                    // Other Success
                    do {

                        let data = response.data.isEmpty ? Data("{}".utf8) : response.data
                        let genericModel = try JSONDecoder().decode(decodingType, from: data)
                        
                        print("\n--RS: \(genericModel)")
                        completion(.success(genericModel))
                    } catch(let error) {
                        print("\n--RS: JSON Parsing Failure")
                        print(error)
                        print(error.localizedDescription)
                        completion(.failure(APIError(error: .internalServerError)))
                    }
                } else {
                    print("\n--RS: Request Failed - \(response.statusCode) - \(response.debugDescription)")
                    completion(.failure(APIError(errorCode: response.statusCode, networkingAPI: target)))
                }
            case .failure(let moyaError):

                // Failure
                print("\n--RS: Request Failed - \(moyaError.localizedDescription)")

                switch moyaError {
                case .underlying(let error, nil):

                    // Parse MoyaError to URLError.Code
                    switch URLError.Code(rawValue: error._code) {
                    case .notConnectedToInternet:
                        completion(.failure(APIError(error: .network)))
                    default:
                        completion(.failure(APIError(error: .generic)))
                    }
                default:
                    completion(.failure(APIError(error: .generic)))
                }
            }
        }
    }
}
