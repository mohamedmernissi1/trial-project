//
//  APIError.swift
//  trutify-dev
//
//  Created by Alexandre Costa on 20/05/2022.
//

import Foundation

struct APIError: Error {

    // MARK: - Properties

    private var apiService: String?
    private var error: HTTPError?

    // MARK: - Default Error

    let defaultTitle = "errorTitle.generic.1"
    let defaultMessage = "errorMessage.generic.1"

    // MARK: - Init

    /// Init error per service
    /// - Parameters:
    ///   - errorCode: The request response code
    ///   - networkingAPI: The target service
    init(errorCode: Int, networkingAPI: NetworkingAPI) {
        self.error = HTTPError(rawValue: errorCode) ?? HTTPError.generic
        self.apiService = self.error == .generic ? "generic" : networkingAPI.service
    }

    /// Use only for generic error type
    /// - Parameter error: HTTPError
    init(error: HTTPError) {
        self.error = error
        self.apiService = "generic"
    }

    // MARK: - Public Interfaces

    var httpError: HTTPError {
        error ?? HTTPError.generic
    }

    var localizedTitle: String {
        // swiftlint:disable:next force_unwrapping
        let formatedKey = String(format: "errorTitle.%@.%d", arguments: [apiService!, error!.rawValue])
        return NSLocalizedString(formatedKey, value: defaultTitle, comment: "Default Error")
    }

    var localizedMessage: String {
        // swiftlint:disable:next force_unwrapping
        let formatedKey = String(format: "errorMessage.%@.%d", arguments: [apiService!, error!.rawValue])
        return NSLocalizedString(formatedKey, value: defaultMessage, comment: "Default Error")
    }
    
    var debugMessage: String {
        guard let error = error else { return "" }
        return "Error::\(error.rawValue) - \(localizedTitle) - \(localizedMessage)"
    }
    
}
