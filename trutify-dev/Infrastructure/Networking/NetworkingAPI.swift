//
//  NetworkingAPI.swift
//  trutify-dev
//
//  Created by Alexandre Costa on 20/05/2022.
//

import Foundation
import Moya

// MARK: - Networking API

enum NetworkingAPI {

    // MARK: - OAuth Endpoints
    case getToken(req: TokenRequest)
    case getRewards(id: String)

    // MARK: - Service name for localized error
    var service: String {
        switch self {
        case .getToken: return "getToken"
        case .getRewards: return "getRewards"
        }
    }
}

// MARK: - Networking API Conforming to TargetType
extension NetworkingAPI: TargetType {
    // MARK: - Setting Base URL
    var baseURL: URL {
            URL(string: NetworkingAPIConfiguration().host)!
    }
    
    // MARK: - Setting Request Paths
    var path: String {
        switch self {
        case .getToken:
            return NetworkingAPIConfiguration().tokens
        case .getRewards:
            return NetworkingAPIConfiguration().rewards
        }
    }
    
    // MARK: - Selecting HTTP Method
    var method: Moya.Method {
        switch self {
        default:
            return .post
        }
    }
    
    var sampleData: Data {
        Data()
    }
    
    // MARK: - Setting Parameters
    var task: Task {
        switch self {
        case .getToken(var req):
            req.client_id = NetworkingAPIConfiguration().clientId
            req.client_secret = NetworkingAPIConfiguration().clientSecret
            return .requestJSONEncodable(req)
        case .getRewards(let id):
            return .requestParameters(parameters: ["id": id], encoding: URLEncoding.queryString)
//        default:
//            return .requestPlain
        }
    }
    
    // MARK: - Setting Headers
    var headers: [String: String]? {
        var _: [String: String] = [HeaderField.contentType.rawValue: MediaType.json.rawValue]

        switch self {
        case .getToken:
            return [HeaderField.contentType.rawValue: MediaType.formURLEncoded.rawValue]
        case .getRewards:
            return [HeaderField.contentType.rawValue: MediaType.formURLEncoded.rawValue]
//        default:
//            defaultHeader[HeaderField.authorization.rawValue] = "Bearer \(UserSession.shared.apiAccessToken ?? "")"
//            return defaultHeader
        }
    }
}
