//
//  HTTPError.swift
//  trutify-dev
//
//  Created by Alexandre Costa on 20/05/2022.
//

import Foundation

enum HTTPError: Int {
    
    // ⚠️ Please keep order by error code, and respect the naming convention of HTTP Status Codes from the url bellow ⚠️
    // https://www.restapitutorial.com/httpstatuscodes.html
    
    // MARK: - Custom App Errors
    
    case network = 0
    case generic = 1
    
    // MARK: - HTTP Errors
    
    case unauthorized = 401
    case forbidden = 403
    case notFound = 404
    case conflict = 409
    case internalServerError = 500
    case badGateway = 502
}
