//
//  Singleton.swift
//  trutify-dev
//
//  Created by Alexandre Costa on 20/05/2022.
//

import Foundation

/// Singleton protocol.
protocol Singleton {
    
    /// Shared instance.
    static var shared: Self { get }
}
