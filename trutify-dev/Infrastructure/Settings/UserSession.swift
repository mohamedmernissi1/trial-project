//
//  UserSession.swift
//  trutify-dev
//
//  Created by Alexandre Costa on 20/05/2022.
//

import Foundation
import Foundation
import UIKit

final class UserSession: Singleton {
    
    // MARK: - Singleton
    
    /// Settings shared instance
    static let shared = UserSession()
    
    // MARK: - Init
    
    /// Prevent instance creation.
    private init() {}
    
    // MARK: - Private Properties
    
    private var userDefaults: UserDefaults { UserDefaults.standard }
    
    // MARK: Tokens
    
    var apiAccessToken: String? {
        get { userDefaults.get(.apiAccessToken) }
        set { userDefaults.set(.apiAccessToken, to: newValue ?? "") }
    }
    
    var apiAccessTokenExpirationDate: Date? {
        get { userDefaults.get(.apiAccessTokenExpirationDate) }
        set { userDefaults.set(.apiAccessTokenExpirationDate, to: newValue ?? Date()) }
    }
    
    var apiRefreshToken: String? {
        get { userDefaults.get(.apiRefreshToken) }
        set { userDefaults.set(.apiRefreshToken, to: newValue ?? "") }
    }
    
    var userId: String? {
        get { userDefaults.get(.userId) }
        set { userDefaults.set(.userId, to: newValue ?? "") }
    }
    
    // MARK: App Mode
    
    var appView: AppView {
        get { AppView(rawValue: userDefaults.get(.appView) ?? "") ?? .home }
        set { userDefaults.set(.appView, to: newValue.rawValue) }
    }
    
    // MARK: - Methods
    
    static func saveSession(_ token: Token) {
        self.shared.apiAccessToken = token.accessToken
        self.shared.apiRefreshToken = token.refreshToken
        self.shared.userId = token.userId
        
        let expiresIn = Date(timeIntervalSinceNow: token.expiresIn ?? 3600)
        self.shared.apiAccessTokenExpirationDate = expiresIn
    }
    
    static func signOut() {
        self.shared.userDefaults.clearAll()
    }
    
}
