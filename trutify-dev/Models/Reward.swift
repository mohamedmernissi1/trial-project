//
//  Reward.swift
//  trutify-dev
//
//  Created by mohamed mernissi on 1/6/2022.
//

import Foundation

struct Reward: Codable {
    let id: String?
    let type: String?
    let userID: String?
    let points: Int?
    let value: Int?
    let brandID: String?
    let locationID: String?

    enum CodingKeys: String, CodingKey {
        case id
        case type
        case userID
        case points
        case value
        case brandID
        case locationID
    }
}
