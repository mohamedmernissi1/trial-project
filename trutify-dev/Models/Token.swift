//
//  Token.swift
//  trutify-dev
//
//  Created by Alexandre Costa on 20/05/2022.
//

import Foundation

struct Token: Codable {
    let accessToken: String
    let refreshToken: String?
    let tokenType: String?
    let expiresIn: TimeInterval?
    let userId: String?

    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case refreshToken = "refresh_token"
        case tokenType = "token_type"
        case expiresIn = "expires_in"
        case userId = "user_id"
    }
    
}

struct TokenRequest: Codable {
    var grant_type: GrantType = URLRequest.GrantType.authorizationCode
    var client_id: String?
    var client_secret: String?
    var code: String?
    var redirect_uri: String?
    
    init(_code: String, redirect: String){
        code = _code
        redirect_uri = redirect
    }
}
