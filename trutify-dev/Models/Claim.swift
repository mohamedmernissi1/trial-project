//
//  Claim.swift
//  trutify-dev
//
//  Created by mohamed mernissi on 1/6/2022.
//

import Foundation

struct Claim: Codable {
    let systemMessage: String?
    let operationID: String?
    let userMessage: String?
    let code: String?

    enum CodingKeys: String, CodingKey {
        case systemMessage
        case operationID
        case userMessage
        case code
    }
}


struct ClaimRequest: Codable {
    let userID: String?
    let type: String?
    let status: String?
    let reward: String?

    enum CodingKeys: String, CodingKey {
        case userID
        case type
        case status
        case reward
    }
}
