//
//  SplashView.swift
//  trutify-dev
//
//  Created by Alexandre Costa on 20/05/2022.
//

import SwiftUI

struct SplashView: View {
    @EnvironmentObject private var viewRouter: ViewRouter
    
    var body: some View {
        Image("purpleTrutifyLogo").onAppear(perform: {
            self.gotoHome(time: 2.5)
        })
    }
    
    func gotoHome(time: Double){
        DispatchQueue.main.asyncAfter(deadline: .now() + Double(time)) {
            viewRouter.currentView = .home
        }
    }
}

struct SplashView_Previews: PreviewProvider {
    static var previews: some View {
        SplashView()
    }
}
