//
//  HomeView.swift
//  trutify-dev
//
//  Created by Alexandre Costa on 20/05/2022.
//

import SwiftUI

struct HomeView: View {
    @State private var isActive: Bool = false

    var body: some View {
        VStack {
            HStack
            {
                Image("contributionIcon")
                    .resizable()
                    .frame(width: 20, height: 20)
                    .aspectRatio(contentMode: .fit)
                    .padding(.init(top: 0, leading: 24, bottom: 0, trailing: 0))
                
                Spacer()
                
                Image("rewardIcon")
                    .resizable()
                    .frame(width: 20, height: 20)
                    .aspectRatio(contentMode: .fit)
                    .padding(.init(top: 0, leading: 0, bottom: 0, trailing: 24))
                
            }
            
            
            Image("loveLocal")
                .resizable()
                .frame(width: 115, height: 40)
                .padding(.init(top: 40, leading: 0, bottom: 60, trailing: 0))
            
            FeedbackRequestBox(brand: "Picnic Foods", entriesSoFar: 65, targetEntries: 100)
                .padding(.init(top: 0, leading: 0, bottom: 30, trailing: 0))
            
            Text("Your first 10 entries will earn you £10")
                .foregroundColor(Color("truRed"))
                .font(Font.custom("Sailec", size: 12))
                .padding(.init(top: 0, leading: 0, bottom: 75, trailing: 0))
            
            Text("Start Feedback")
                 .foregroundColor(Color("truPurple"))
                 .font(Font.custom("Sailec Medium", size: 16))
        }
    }
}
struct SwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
