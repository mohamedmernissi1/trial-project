//
//  EntriesProgressView.swift
//  trutify-dev
//
//  Created by Alexandre Costa on 23/05/2022.
//

import SwiftUI

struct EntriesProgressView: View {
    var entriesSoFar: Double
    var targetEntries: Double
    var body: some View {
        VStack {
            let left = Int (targetEntries - entriesSoFar)
            Text("**\(left)** people submitted feedback ")
                .foregroundColor(Color("truRed"))
                .font(Font.custom("Sailec", size: 14))
           
            ProgressView(value: self.entriesSoFar, total: self.targetEntries)
                .scaleEffect(x: 1, y: 2, anchor: .center)
                .progressViewStyle(LinearProgressViewStyle(tint: Color("truRed")))
        }
    }
}

struct ProgressView_Previews: PreviewProvider {
    static var previews: some View {
        EntriesProgressView(entriesSoFar: 5, targetEntries: 10)
    }
}
