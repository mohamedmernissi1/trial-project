//
//  FeedbackRequestBox.swift
//  trutify-dev
//
//  Created by Alexandre Costa on 23/05/2022.
//

import SwiftUI

struct FeedbackRequestBox: View {
    let brand : String
    let entriesSoFar: Double
    let targetEntries: Double
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 10, style: .continuous)
                .fill(.white)
                .frame(width: 327, height: 264)
                .shadow(radius: 5, x: 1, y: 3)
             
            VStack {
                Text(self.brand)
                    .font(Font.custom("Sailec Medium", size: 16))
                
                Divider()
                    .frame(maxWidth: 100)
                  .padding(.all)
                
       
                EntriesProgressView(entriesSoFar: self.entriesSoFar,
                                    targetEntries: self.targetEntries)
                .frame(width: 279, height: 10)
                
                Text("Be rewarded for every entry")
                    .font(Font.custom("Sailec Medium", size: 12))
                    .padding(.all)
                Text("Plus once 100 entries reach at this place,\none person wins £50")
                    .font(Font.custom("Sailec", size: 12))
                    .multilineTextAlignment(.center)
                    
            }
        }
    }
}
struct FeedbackRequestBox_Previews: PreviewProvider {
 
    static var previews: some View {
        FeedbackRequestBox(brand: "Lincoln Coffee House",
                           entriesSoFar: 5,
                           targetEntries: 100)
    }
}
