//
//  PointsBoxView.swift
//  trutify-dev
//
//  Created by mohamed mernissi on 31/5/2022.
//

import SwiftUI

struct PointsBoxView: View {
    let points: String
    let price: String
    let entriesSoFar: Double
    let targetEntries: Double
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 10, style: .continuous)
                .fill(.white)
                .frame(width: 327, height: 264)
                .shadow(radius: 5, x: 1, y: 3)
             
            VStack {
                Text("You won")
                    .font(Font.custom("Sailec", size: 16))
                    .padding(.pi)
                
                HStack {
                    Text(self.points)
                        .font(Font.custom("Sailec Medium", size: 40))
                    
                    Text("pts")
                        .font(Font.custom("Sailec Medium", size: 12))
                        .multilineTextAlignment(.center)
                }
                .padding(.pi)
                
                Text(self.price)
                    .font(Font.custom("Sailec", size: 20))
                Divider()
                    .frame(maxWidth: 327)
                    .padding(.all)
                
                Text("As part of the feedback draw by")
                    .font(Font.custom("Sailec", size: 16))
                    .padding(.pi)
                Text("Pret A Manger")
                    .font(Font.custom("Sailec Medium", size: 16))
                
            }
        }
    }
}

struct PointsBoxView_Previews: PreviewProvider {
    static var previews: some View {
        PointsBoxView(points: "75000", price: "(£50.00)",
    entriesSoFar: 5,
    targetEntries: 100)
    }
}
