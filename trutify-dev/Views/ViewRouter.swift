//
//  ViewRouter.swift
//  trutify-dev
//
//  Created by Alexandre Costa on 20/05/2022.
//

import Foundation

enum AppView: String {
    case home, splash, rewards
}

class ViewRouter: ObservableObject {
    @Published var currentView: AppView = .splash
}
