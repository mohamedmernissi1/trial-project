//
//  ContentView.swift
//  trutify-dev
//
//  Created by Alexandre Costa on 20/05/2022.
//

import SwiftUI

struct ContentView: View {
    @EnvironmentObject private var viewRouter: ViewRouter
    
    var body: some View {
        VStack {
            switch viewRouter.currentView {
            case .splash: SplashView()
            case .home: HomeView()
            case .rewards: RewardsView()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
