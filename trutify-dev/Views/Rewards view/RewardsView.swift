//
//  RewardsView.swift
//  trutify-dev
//
//  Created by mohamed mernissi on 31/5/2022.
//

import SwiftUI

struct RewardsView: View {
    var body: some View {
        VStack {
            
            VStack {
                Image("Trophy icon w padding")
                    .resizable()
                    .frame(width: 80, height: 80)
                    .aspectRatio(contentMode: .fit)
                
                Text("Congratulations!")
                    .font(Font.custom("Sailec Medium", size: 20))
                    .padding(.bottom)
            }
            
            PointsBoxView(points: "75000", price: "(£50.00)",
                          entriesSoFar: 5,
                          targetEntries: 100)
            .padding(.top)
        }
    }
}

struct RewardsView_Previews: PreviewProvider {
    static var previews: some View {
        RewardsView()
    }
}
