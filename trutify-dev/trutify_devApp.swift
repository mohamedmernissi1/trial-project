//
//  trutify_devApp.swift
//  trutify-dev
//
//  Created by Alexandre Costa on 20/05/2022.
//

import SwiftUI

@main
struct trutify_devApp: App {
    @StateObject private var viewRouter = ViewRouter()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(viewRouter)
                .onOpenURL { URL in
                    print("opened", URL)
                }
        }
    }
}
