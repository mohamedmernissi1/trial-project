//
//  AuthService.swift
//  trutify-dev
//
//  Created by Alexandre Costa on 20/05/2022.
//

import Foundation

struct AuthService {
    static func login(fromURL authURL: URL, completion: @escaping (Result<Token, APIError>) -> Void) {
        let url = URLComponents(url: authURL, resolvingAgainstBaseURL: true)!
        let code = url.queryItems?.first(where: { $0.name == "code" })?.value
        
        let body = TokenRequest(_code: code!, redirect: authURL.absoluteString)

        // Init the API and send the request
        let request: NetworkingAPI = .getToken(req: body)
        
        // Send the request
        NetworkingAdapter().request(target: request, decodingType: Token.self) { result in
            switch result {
            case .success(let token):
                UserSession.saveSession(token)
                completion(.success(token))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
